include_recipe 'docker::start'


Chef::Log.info("*************app*****************")
appserver = search(:node, "role:deploy").first
Chef::Log.info(appserver)
Chef::Log.info("*************node*****************")
Chef::Log.info(node["deploy"])
Chef::Log.info(node["deploy"]['test_app1'])
Chef::Log.info(node["deploy"]['test_app1']['environment_variables'])
Chef::Log.info("*************end*****************")
env_vars = node["deploy"]['test_app1']['environment_variables']

execute "docker login" do
  command "docker login -u #{env_vars[:user_name]} -p '#{env_vars[:password]}'"
  action :run
end

execute "pull docker container" do
 command "docker pull #{env_vars[:docker_image]}:#{env_vars[:tag] || 'latest'}"
 action :run
end

execute "down the existing container" do
 command "docker stop test_mongo || true && docker rm test_mongo || true"
 action :run
end

execute "up docker container container" do
 command "docker run -d --name test_mongo #{env_vars[:docker_image]}:#{env_vars[:tag] || 'latest'}"
 action :run
end
